FROM golang:1.19.1-buster AS builder

WORKDIR /app

COPY go.mod go.mod
COPY go.sum go.sum
RUN go mod download

COPY . .

RUN go build ./cmd/app

FROM ubuntu:20.04

WORKDIR /app
COPY --from=builder /app/app .
CMD ["/app/app"]
