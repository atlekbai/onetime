# onetime

## Setup

```bash
docker compose pull
docker compose build
docker compose up -d
```

## Shutdown

```bash
docker compose down
```
